# DS Framework

Simple projekt framework writed in clear PHP 7, you can find there used cache, controllers, routing and many more :)

# Configuration

## Add track for routing in file 
* src/class/track.php

## Change database data
* config/config.ini

## Create controller
* src/controller/xxx.php

## Add template 
* template/xxx.html.php

# Unit test 
### Example
run command : composer run test_win