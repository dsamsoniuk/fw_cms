<html>

<head>
    <meta charset="UTF-8">
    <meta name="description" content="Free Web tutorials">
    <meta name="keywords" content="HTML,CSS,XML,JavaScript">
    <meta name="author" content="John Doe">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php  translate::get('main.title'); ?></title>

    <link rel="stylesheet" href=" <?php writeUrl('assets/css/lib/bootstrap-theme.min.css');?> ">
    <link rel="stylesheet" href=" <?php writeUrl('assets/css/lib/bootstrap.min.css');?> ">
    <link rel="stylesheet" href=" <?php writeUrl('assets/css/style.css');?> ">
    <link rel="stylesheet" href=" <?php writeUrl('assets/css/login.css');?> ">
    <script src=" <?php writeUrl('assets/js/lib/jquery.min.js');?>"></script>
    <script>
        window.main_dir = '<?php writeUrl(''); ?>';
    </script>
</head>
<body>
