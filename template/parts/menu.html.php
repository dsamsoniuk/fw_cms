<nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          
          <ul class="nav navbar-nav">
          <li><a class="navbar-brand" href="<?php writeUrl(''); ?>"><?php  translate::get('basic.strona.glowna'); ?></a></li>
              <li><a class="" href="<?php writeUrl('main/22/22'); ?>">tests</a></li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true"><?php  translate::get('basic.menu'); ?> <span class="caret"></span></a>
                <ul class="dropdown-menu">
                  <li><a href="<?php writeUrl('translation'); ?>"><?php  translate::get('basic.tlumaczenia'); ?></a></li>
                  <!-- <li><a href="#">Another action</a></li>
                  <li><a href="#">Something else here</a></li>
                  <li role="separator" class="divider"></li>
                  <li class="dropdown-header">Nav header</li>
                  <li><a href="#">Separated link</a></li>
                  <li><a href="#">One more separated link</a></li> -->
                </ul>
              </li>
            
          
          </ul>
         
        </div>
        <div id="navbar" class="navbar-right">

          <?php
            if (session::userIsLogged()) {
              file::render(array(
                'name'=> 'menu_logged',
                'controller'=> 'src/controller/parts/menu.logged.php', 
                'request' => [] 
              )); 
            } else {
              file::render(array(
                'name'=> 'menu_unlogged',
                'controller'=> 'src/controller/parts/menu.unlogged.php', 
                'request' => [] 
              ));
            }
          ?>

        </div><!--/.navbar-collapse -->
      </div>
    </nav>