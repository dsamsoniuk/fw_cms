
<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <center>
                <h2><?php  translate::get('basic.tlumaczenia'); ?></h2>
            </center>
        </div>
    </div>
</div>
<hr>
<?php // translate::get('main.pierwszytekst'); ?>


<div class="container">
    <div class="row">
        <div class="col-sm-12">
        <h3>Dodaj tłumaczenie</h3> <hr>
        <?php write($vars['translation']['form']); ?>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-sm-12">
        <h3>Lista tłumaczeń</h3><hr>
        <?php 
            $t = new table();
            $t->setRowHead([['Nazwa', 5], ['Zawartosc', 5], ['',1], ['',1]]);
        
            foreach ($vars['translation']['tran_content'] as $row) {
                $btn_add = '<button class="btn btn-primary" onclick="trans.edit(this)">Edytuj</button>';
                $btn_del = '<button class="btn btn-danger" onclick="trans.delete(this, '.$row['id'].')">Usuń</button>';
                $t->setRowBody([ 
                    ['content' => $row['name'] ], 
                    ['content' => $row['content'] , 'class' => 'trans_content', 'id' => 'content_trans_'.$row['id'] ], 
                    ['content' => $btn_add ], 
                    ['content' => $btn_del ]
                ]);
            }

            write( $t->generate() ); 
        ?>
        </div>
    </div>
</div>
