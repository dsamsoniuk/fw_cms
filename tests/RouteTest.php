<?php

use PHPUnit\Framework\TestCase;
// include_once './src/class/file.class.php';
// file::includeDir('src/class','php');
// file::includeDir('src/entity','php');
// file::includeOnce('./define.php');

class RouteTest extends TestCase
{

    public function testLookingForController()
    {
        $r = new route;
        $this->assertSame(array(
            'track'         => 'login',
            'name'          => 'login',
            'controller'    => 'src/controller/login.php',
            'regex'         => '/login\/?/i'
        ), $r->findController('/login'));

        $this->assertEmpty( $r->findController('/not_existed_url') );
    }
 
    public function testPrepareUrlRegexToControllers(){
        $r              = new route;
        $controllers    = $r->prepareUrlRegex();

        $this->assertSame(
            array(  "/^\/\/?/i" , "/main\/[0-9]+\/[a-zA-Z0-9]+\/?/i" , "/login\/?/i" , "/logout\/?/i" , "/register\/?/i" ),
            $r->prepareUrlRegex()
        );
    }

    public function testGetDataFromRequestAddress(){
        $r              = new route;
        $track          = '/main/34/test';
        $controller     = $r->findController($track);
        $this->assertSame(
            array("id" => "main" ,"name" => "34"  ),
            $r->getRequests($controller, $track)
        );
    }



}