<?php

use PHPUnit\Framework\TestCase;

include './src/class/file.class.php';
file::includeDir('src/class','php');
file::includeDir('src/entity','php');
file::includeOnce('./define.php');


class SecureTest extends TestCase {

    // public function __construct() {
    // }
    // public function testEncryptionPassword(){
    //     $secure = new Secure;

    //     $this->assertEquals(
    //         'dae3d1fb54406a5eb2e5527533563e1e78b77341fee5ec82912105ff384624f7',
    //         $secure->password('test')
    //     );
    // }

    public function testStringSave(){
        $secure = new Secure;

        $this->assertSame('name', $secure->saveString('name'));
        $this->assertSame('232', $secure->saveString(232));
        $this->assertSame('', $secure->saveString());

        $name   = 'test343<b></b>';
        $this->assertSame('test343&lt;b&gt;&lt;/b&gt;', $secure->saveString($name));
    
        $name   = 'What does "yolo" mean?';
        $this->assertSame('What does \&quot;yolo\&quot; mean?', $secure->saveString($name));
        return $secure;
    }
    /**
     * @depends testStringSave
     */
    public function testEncryptionPassword($secure){
        $this->assertSame(
            'dae3d1fb54406a5eb2e5527533563e1e78b77341fee5ec82912105ff384624f7',
            $secure->password('test')
        );
    }
    /**
     * @depends testStringSave
     */
    public function testCheckEmailValidate($secure){
        $this->assertSame(
            false,
            $secure->isValidateEmail('')
        );

        $this->assertSame(
            true,
            $secure->isValidateEmail('test@test.pl')
        );

    }
}