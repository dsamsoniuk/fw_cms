<?php


declare(strict_types=1);

include './src/class/request.class.php';

use PHPUnit\Framework\TestCase;

final class RequestTest extends TestCase
{
    public function testParseInt(): void
    {
        $this->assertSame(0, request::getInt('test'));

        $_POST['number']    = '3';
        $this->assertSame(3, request::getInt('number'));
        
        $_POST['number']    = '';
        $this->assertSame(0, request::getInt('number'));

        unset($_POST['number']);
        $_GET['number']     = '3';
        $this->assertSame(3, request::getInt('number'));

        $_GET['number']     = '3lala';
        $this->assertSame(3, request::getInt('number'));

    }

    // public function testParseString(): void
    // {

    //     $this->assertSame('', request::getString('name'));

    //     $_POST['name']  = 'test343<b></b>';
    //     $this->assertSame('test343&lt;b&gt;&lt;/b&gt;', request::getString('name'));

    //     $_POST['name']  = 'What does "yolo" mean?';
    //     $this->assertSame('What does \&quot;yolo\&quot; mean?', request::getString('name'));
        
    //     unset($_POST['name']);
    //     $_GET['name']   = 'test';
    //     $this->assertSame('test', request::getString('name'));

    // }
}