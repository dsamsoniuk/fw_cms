<?php

/**
 * Delete translation
 */
 function translation_delete (array $inputs) {

    $id   = request::getString('id');

    $trans = new translation();
    $trans->limit(1);
    $trans->where('id','=', $id);
    $r = $trans->select();

    $info = '';
    $success = true;
    if (!empty($r['result'])) {
        $trans->delete();
        $info = 'Tłumaczenie usunięte';
        $success = true;
    } else {
        $info = 'Brak takiego tłumaczenia';
    }

    echo json_encode( array(
        'info' => $info, 
        'success' => $success 
    ) );
};

/**
 * Update translation
 */
function translation_update (array $inputs) {

    $id         = request::getInt('id');
    $content    = request::getString('content');

    $trans      = new translation();
    $trans->limit(1);
    $trans->where('id','=', $id);
    $r          = $trans->select();

    $info       = '';
    $success    = true;

    if (!empty($r['result'])) {
        $trans->update(array(
            'content' => $content
        ));
        $info = 'Tłumaczenie zaktualizowane';
        $success = true;
    } else {
        $info = 'Brak takiego tłumaczenia';
    }

    echo json_encode( array(
        'info' => $info, 
        'success' => $success 
    ) );
};
