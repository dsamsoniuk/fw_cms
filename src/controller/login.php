<?php

/**
 * Controller login
 */
function login(array $inputs) : array {

    $login      = request::getString('username');
    $password   = request::getString('password');
    
    if ($login && $password) {
        $u       = new user();
        $u->where('login','=',$login);
        $u->where('password','=',secure::password($password));
        $u->limit(1);
        $usr     = $u->select();

        if (!empty($usr['result'])) {
            alert('Zostales zalogowany', 'success');
            $user           = $usr['result'][0];
            $user['roles']  = $u->getRoles();
            // Tworzenie sesji
            $session = new session;
            $session->set('user', $user);
            route::redirect('/');

        } else {
           // Nie ma usera
           alert('Nie ma takiego uzytownika', 'danger');
       }
    } 

    return array('template/login.html.php', array());

};

