<?php

/**
 * Controller register
 */
function register(array $inputs) : array {

    $login      = request::getString('login');
    $password   = request::getString('password');
    $username   = request::getString('username');
    $email      = request::getString('email');

    if ($login && $password ) {

        if (secure::isValidateEmail($email)) {

            $user_find  = new user();
            $user_find->limit(1);
            $user_find->where('login','=', $login);
            $user_find->where('email','=', $email);
            $r  = $user_find->select();
            // isValidateEmail
            if ($user_find->count) {
                /**
                 * User exists
                 */
                alert('Użytkownik już istnieje', 'danger');
            } else {
                /**
                 * Register
                 */
                $u          = new user;
                $new_user_data = array(
                    'login'     => $login,
                    'password'  => secure::password($password),
                    'username'  => $username,
                    'email'     => $email
                );
                $u->insert($new_user_data);

                $nu         = new user;
                $nu->limit(1);
                $nu->where('login','=', $login);
                $nu->where('password','=', secure::password($password));
                $nu_res     = $nu->select('id');
                $user_id    = $nu_res['result'][0]['id'];

                $role       = new role;
                $role->limit(1);
                $role->where('name','=','user');
                $role_res   = $role->select('id');

                $ur         = new user2role;
                $ur->insert(array(
                    "user_id" => $user_id,
                    'role_id' => $role_res['result'][0]['id']
                ));
                alert('Zostałeś zarejestrowany', 'success');
            }
        } else {
            alert('Dane nie poprawne', 'danger');
        }
    }

    $f = new form("register","POST");
    $f->setInput("login", "text", "Login");
    $f->setInput("username", "text", "Nazwa użtkownika");
    $f->setInput("password", "password", "Hasło");
    $f->setInput("email", "email", "E-mail");
    $f->setInput("zarejestruj się", "submit", "", "");



    return array('template/register.html.php', array(
        'form' => $f->generate()
    ));

};