<?php

// Controller
 function translation (array $inputs) {

    if (request::get('name') && request::get('content')) {
        $lang = (request::get('lang')) ? request::get('lang') : config::value('default_lang');
        $trans = new translation();
        $trans->where('name', '=', request::get('name'));
        $trans->where('lang', '=', $lang);
        $trans->limit(1);
        $rows = $trans->select();

        if (empty($rows['result'])) {
            $nt       = new translation();
            $nt->insert([
                'name' => request::get('name'),
                'content' => request::get('content'),
                'lang' => $lang
            ]);
            alert('Nowe tłumaczenie dodane - '.request::get('name'), 'success');
        }
    }

    $trans       = new translation();
    $res     = $trans->select();

    // FORMULARZ DODAWANIA TRESCI
    $f = new form("translation","POST");
    $f->setInput("name", "text", "Nazwa");
    $f->setInput("content", "text", "Zawartość");
    $f->setInput("lang", "text", "Język", config::value('default_lang'));
    $f->setInput("Dodaj nowy tekst", "submit", "", "");

    return array('template/translation.html.php', array(
        'form'          => $f->generate(),
        'tran_content'  => $res['result']
    ));

};