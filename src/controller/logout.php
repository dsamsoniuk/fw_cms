
<?php

/**
 * Logout user
 */
function logout() {
    session::destroy();
    route::redirect('/');
    // return array('template/base.html.php', array());
}