<?php


/**
 * Dodatkowe skladniki dla wyciagania danych z bazy
 */

interface db_prop {
   public function where(string $name, string $eq, string $val);
   public function limit(int $limit);
   public function select(string $prop, array $data);
}

abstract class db_property implements db_prop {

    public $where = [];
    public $limit = 0;
    public $order = '';
    /**
     * Get scope of data
     */
    public function where (string $name, string $eq, string $val) : void {
        if (strtolower($eq) == 'in') {
            $this->where[] = $name.' '.$eq." ".$val."";
        } else {
            $this->where[] = $name.' '.$eq." '".$val."'";
        }
    }

    public function query(string $query, array $data = []) {
        // try {
        //     $stmt   = $this->connection->prepare($query);
        //     $stmt->execute($data);
        //     $result = $stmt->fetchAll();
        // } catch (PDOException $e) {
        //     print "Error!: " . $e->getMessage() . "<br/>";
        //     die();
        // }
        // return $result;
    }

    /**
     * Return selected data
     */
    public function select (string $prop = "*", array $data = []) : array {

        $table  = $this->table;
        $stmt   = [];

        $q      = 'SELECT '.$prop.' FROM '.$table;

        if (!empty($this->where)) {
            $q .= ' WHERE '.implode(' AND ', $this->where);
        }
       
        if ($this->order) {
            $q .= $this->order;
        }

        if ($this->limit) {
            $q .= ' LIMIT '.$this->limit;
        }

        try {
            $stmt   = $this->connection->prepare($q);
            $stmt->execute($data);
            $result = $stmt->fetchAll();
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }

        $this->result       = $result;
        $this->query        = $q;
        $this->count        = count($result);
        return [
            "query"     => $q,
            "result"    => $result
        ];
    }

    /**
     * Set limit selected data
     */
    public function limit (int $limit) : void {
        $this->limit = $limit ?? 0;
    }

    /**
     * Set order data by column
     */
    public function order (string $order) : void {
        $this->order = ' order by '.$order ?? 0;
    }

    /**
     * Aktualizuj wybrane wiersze
     */
    public function update(array $attr) {
        $ids    = [];
        $q      = 'UPDATE '.$this->table.' SET ';
        $result = self::select("id");

        foreach ($result['result'] as $v) {

            $ids[] = $v['id'];
        }
        foreach ($attr as $i => $v) {
            $q .= '`'.$i.'`'.'="'.$v.'"';
        }

        $q .= ' WHERE id in ('.implode(',',$ids).')';

        try {
            $stmt   = $this->connection->prepare($q);
            $stmt->execute([]);
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }
    
    /**
     * Usun wybrane wiersze
     */
    public function delete() {
        $ids    = [];
        $result = self::select("id");

        foreach ($result['result'] as $v) {
            $ids[] = $v['id'];
        }
        $q      = 'DELETE FROM '.$this->table.' WHERE id in ('.implode(',',$ids).')';

        try {
            $stmt   = $this->connection->prepare($q);
            $stmt->execute([]);
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }
    public function insert(array $data) : void {

        $q = 'INSERT INTO '.$this->table.' ';
        $cols = array();
        $vals = array();
        foreach ($data as $i => $v) {
            $cols[] = "`".$i."`";
            $vals[] = ":".$i."";
        }
        $q .= '('. implode(',',$cols). ') ';
        $q .= ' VALUES ';
        $q .= '('. implode(',',$vals). ') ';

        try {
            $stmt   = $this->connection->prepare($q);
            $stmt->execute($data);
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }
}

abstract class db extends db_property {

    protected $connection;
    protected $table;

    function __construct() {
        $instance           = database::inst();
        $this->table        = get_class($this);
        $this->connection   = $instance->getConnection();
    }

}