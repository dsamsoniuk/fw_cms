<?php


class session {
    
    /**
     * Get data from session
     */
    public function get(string $name) : array {
        return $_SESSION[$name] ?? [];
    }

    /**
     * Set data session
     */
    public function set(string $name, array $data) : void {
        $_SESSION[$name] = $data;
    }

    public static function getCurrentUser() {
        $session = new self;
        return $session->get('user');
    }

    public function findUserRole(string $r) : bool {
        $session = new self;
        $user = $session->get('user');
        foreach ($user['roles'] as $role) {
            if (isset($role['name']) && $role['name'] == $r) {
                return true;
            }
        }
        return false;
    }

    public function destroy() {
        session_destroy();
        $s = new self;
        $u = new user;
        $s->set('user', $u->getUnloggedUserData());
    }

    public static function userIsLogged() {
        $s = new session;
        $u = $s->get('user');

        return $u['id'] ? true : false;
    }
}