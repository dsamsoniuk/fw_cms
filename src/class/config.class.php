<?php

final class config {
    /**
     * Get data from config.ini
     */
    public static function value (string $name) : string {
        $path_config    = MAIN_DIR.'\config\config.ini';
        $values         = parse_ini_file( $path_config, false);
        return $values[ $name ] ?? '';
    }

    // public function getAllValues (bool $struct = false) : array {
    //     // if (!file_exists(config::$path_config)) {
    //     //     throw new Exception("Plik config nie istnieje");
    //     //     return [];
    //     $values = parse_ini_file( $this->$path_config, false);
    //     return $values;
    // }

}