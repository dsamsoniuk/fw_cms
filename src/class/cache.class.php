<?php

/**
 * Klasa cache zapisz/odczytaj/usun plik z katalogu cache
 */
final class cache {

    private $path_cache = "cache";
    private $format     = '.txt';
    /**
     * Set cache
     */
    public function set (string $name, array $data) : void {
        $file       = fopen($this->path_cache.'/'.$name.$this->format, 'w+');
        $content    = serialize($data);
        fwrite($file, $content, strlen($content));
        fclose($file);
    }
    /**
     * Get data from cache
     */
    public function get (string $name) : array {
        $file_path = $this->path_cache.'/'.$name.$this->format;
        if (!file_exists($file_path)) {
            return [];
        }
        $file       = fopen($file_path, 'r');
        $buff       = "";
        while ( !feof($file) ) {
            $buff .= fread($file, 2048);
        }
        fclose($file);
        return unserialize($buff);
    }
    /**
     * Remove data from cache
     */
    public function clear(string $name) : void {
        $file_path = $this->path_cache.'/'.$name.$this->format;
        if (file_exists($file_path)) {
            unlink($this->path_cache.'/'.$name.$this->format);
        }
    }
}