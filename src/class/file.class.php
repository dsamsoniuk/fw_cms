<?php


final class file {
    /**
     * Include list of files from dir but only from root dir
     */
    public static function includeDir(string $dir, string $format) : array {
        $list = array_filter(file::getList($dir), function($v) : bool {
            return !in_array($v, ['.','..']);
        });
        $list = array_filter($list, function($v) use ($format) : bool {
            return strpos($v, $format);
        });
        foreach ($list as $file) {
            include_once ''.$dir.'/'.$file;
        }
        return $list;
    }
    /**
     * Include one file
     */
    public static function includeOnce(string $path) : void {
        if (file::checkExists($path)) {
            include_once $path;
        } else {
            // echo '';
        }
    }
    /**
     * Get list specified dir
     */
    public static function getList(string $dir): array {
        return scandir($dir);
    }
    /**
     * Render controller with template
     */
    public static function render( array $controller ) : void {
        // if ($controller === '') { // TODO: Domyslny dla glownej moze dodac tutaj ERROR?
        //     $controller = 'base';
        // }
        $name           = $controller['name'];
        $req            = $controller['request'] ?? [];      
        include $controller['controller'];
        $data           = $name( $req );
        if (strpos($controller['controller'], 'RPC') !== false) {
            // Dla RPC nie wywoluj szablonu
        }  else {
            $vars           = $vars ?? [];
            $vars[ $name ]  = $data[1];
            include_once $data[0];
        }
    }
    /**
     * Check file exists
     */
    public static function checkExists(string $path) : bool {
        return file_exists($path);
    }

}