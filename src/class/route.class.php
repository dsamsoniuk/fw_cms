<?php

include_once 'config/track.class.php';

final class route extends track {

    private $types_var  = array(
        "string"    => '[a-zA-Z0-9]+',
        "int"       => '[0-9]+'
    );

    private $content_type = "html";

    /**
     * Find and call render file controller with template
     */
    public function content() : void {
        
        try {
            $url                    = self::getUrl();
            $controller             = self::findController($url);
            $page404                = $this->routing_def['error'];
            $session                = new session;
            if (empty($controller) || (isset($controller['role']) && !$session->findUserRole( $controller['role'] ) ) ) {
                $controller = $page404;
            }
            $request                =  self::getRequests($controller, self::cleanUrl($url));
            if (isset($controller['vars']) && count($request) != count($controller['vars'])) {
                $controller = $page404;
            }
            
        } catch (Exception $e) {
            alert($e->getMessage(), 'danger');
        }

        $controller['request']  = $request;

        if (isset($controller['content_type']) && $controller['content_type'] == 'json') {
            header('Content-type: application/json');

            /**
             * CONTAINER
             */
            // debug($controller);
            file::render($controller);
        } else {
            /**
             * HEAD
             */
            file::includeOnce('template/parts/head.html.php');
            /**
             * TOP MENU
             */

            file::includeOnce('template/parts/menu.html.php');
            /**
             * CONTAINER
             */
            file::render($controller);
            /**
             * FOOTER
             */
            file::includeOnce('template/parts/foot.html.php');
        }

    }

    /**
     * Find controller from current url
     */
    public function findController(string $url) : array {

        $controller     = array();
        $current_path   = $this->cleanUrl($url); 
        $urls           = $this->prepareUrlRegex();
        // TODO : tutaj mozna zastosować cache i zapisywac zrenderowane sciezki
        foreach ($urls as $i => $regx) {
            if (preg_match($regx, $current_path)) {
                $this->routing[$i]['regex'] =  $regx;
                $controller                 = $this->routing[$i];
                break;
            }
        }
        return $controller;
    }
    /**
     * Clean url from unnecessary chars
     */
    public function cleanUrl(string $url) : string {

        $split_path     = explode('/', $url);
        $split_path     = array_filter($split_path, function(string $v) : bool {
            return $v;
        });
        $split_path     = array_values($split_path);

        if (!empty($split_path) && $split_path[0] === ROOT_DIR) {
            unset($split_path[0]);
        }
        $split_path     = array_values($split_path);
        $basic_url      = implode('/', $split_path);
        return $basic_url ? $basic_url : '/';
    }
    /**
     * Get data from url where is value not name 
     */
    public function getRequests(array $controller, string $current_path) : array {

        $variables  = [];

        if (!isset($controller['track'])) {
            return $variables;
        }
        $path_arr   = explode('/', $current_path);
        $track_arr  = explode('/', $controller['track']);

        foreach ($track_arr as $i => $v) {
            if (preg_match('/{.*}/', $v)) {
                $var_name               = str_replace('{','',$v);
                $var_name               = str_replace('}','',$var_name);
                if (!isset($path_arr[ $i ])) {
                    continue;
                }
                $val_url                = explode('?', $path_arr[ $i ]);
                $variables[ $var_name ] = $val_url[0];
            }
        }

        return $variables;
    }

    /**
     * Prepare url to be parsered by regex 
     */
    public function prepareUrlRegex() : array {
        $urls   = [];
        $types  = $this->types_var;
        
        foreach ($this->routing as $path) {

            $track = $path['track'];
            if (isset($path['vars'])) {

                foreach ($path['vars'] as $i => $v) {
                    if (!isset($types[$v])) {
                        throw new Exception('Błąd typu w routingu');
                    }
                    $track = str_replace('{'.$i.'}', $types[$v], $track);
                }
            }
                
            $track_arr  = explode('/', $track);
            $track      = implode("\\/",$track_arr);
            $urls[]     = '/'.$track.'\/?/i';
        }
        return $urls;
    }
    /**
     * Get current URL
     */
    public function getUrl() : string {
        return $_SERVER['REQUEST_URI'];
    }
    /**
     * Redirect to page
     */
    public static function redirect(string $url) {
        header("Location: http://localhost/".ROOT_DIR.$url);
        die();
    }
}