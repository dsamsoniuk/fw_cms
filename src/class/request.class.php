<?php


class request {
    /**
     * Return data from POST and GET
     */
    public static function get(string $name = '') {
        $value = $_POST[ $name ] ?? $_GET[ $name ] ?? '';
        return $value;
    }
    /**
     * Parse int
     */
    public static function getInt(string $name) {
        $val =  request::get($name);
        return intval($val);
    }
    /**
     * Parse string
     */
    public static function getString(string $name) {
        $val =  request::get($name);
        return (string)$val;
    }

    public static function clearPost() {
        $_POST = array();
    }

}