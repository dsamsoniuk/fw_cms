<?php



class secure {
    /**
     * Secure string
     */
    public function saveString(string $s='') : string {
        return htmlspecialchars(addslashes($s));
    }
    /**
     * Encryption password
     */
    public static function password(string $pwd) :string {
        $pwd .= config::value('sol');
        $hash = hash('sha256', $pwd);
        return $hash;
        // return sha1($val);
    }

    public static function isValidateEmail(string $email) : bool {
        $valid = true;
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            // $emailErr = "Invalid email format"; 
            $valid = false;
        }
        return $valid;
    }
}