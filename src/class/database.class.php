<?php
/**
 * Klasa glowna database
 */
 class database  {

    protected $connection;
    private static $instance    = null;

    private $db_name;
    private $host_name;
    private $login;
    private $password;

    public  function __construct () {
        
        $this->host_name    = config::value('host');
        $this->db_name      = config::value('db');
        $this->login        = config::value('login');
        $this->password     = config::value('password');

        try {
            $this->connection = new PDO('mysql:host='.$this->host_name.';dbname='.$this->db_name, $this->login, $this->password );
            // echo 'Połączenie nawiązane!';
        }
        catch(PDOException $e) {
            debug('Połączenie nie mogło zostać utworzone.<br />');
        }
    }
	// Magic method clone is empty to prevent duplication of connection
	private function __clone() { }
    /**
     * Instance current class
     */
    public static function inst() {

        if (self::$instance === null) {
            self::$instance = new self();
        }
        return self::$instance;
    }
    /**
     * Return PDO database connection
     */
    public function getConnection(){
        return $this->connection;
    }

    public function query(string $q, array $data){
        try {
            $stmt   = $this->connection->prepare($q);
            $stmt->execute($data);
            $result = $stmt->fetchAll();
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
        return $result;
    }
}

