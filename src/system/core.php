<?php

include 'src/class/file.class.php';
session_start();
/**
 * ADD ALL IMPORTANT CLASSES
 */
file::includeDir('src/class','php');
file::includeDir('src/entity','php');
file::includeDir('extension','php');
file::includeDir('src/system','php'); // backend files

/**
 * BACKEND 
 */
// Set unlogged user
$session = new session;
$sess   =   $session->get('user');
if (empty($sess) || !$sess['id']) {
    $user   = new user;
    $session->set('user', $user->getUnloggedUserData());
}

$route  = new route;
$route->content();
