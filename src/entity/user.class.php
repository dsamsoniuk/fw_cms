<?php



class user extends db {


    public function getRoles() : array {
        
        $roles  = array();
        $user   = $this->result[0] ?? [];

        if (!empty($user)) {
            $u2r    = new user2role;
            $u2r->where('user_id','=', $user['id']);
            $u2r->select();
            $roles  = $u2r->getByNameRole();
        }
        return $roles;
    }

    public function getUser(string $login, string $password) : bool {
        $user   = new self;
        $user->limit(1);
        $user->where('login','=', $login);
        $user->where('password','=', secure::password($password));
        $r      = $user->select();
        return !empty($r['result']);
    }
    
    public function getUnloggedUserData() : array {
        return array(
            'id'    => 0,
            'roles' => array(''),
            'username' => 'not_logged'
        );
    }
}