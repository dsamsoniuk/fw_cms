<?php



class user2role extends db {

    public function getByNameRole() {
        $roles  = $this->result;
        $result = [];
        if (!empty($roles)) {
            $role_ids   = array();
            foreach ($roles as $v) {
                $role_ids[] = $v['role_id'];
            }
            $role       = new role;
            $role->where('id','IN', '('.implode(',',$role_ids).')');
            $r          = $role->select('name');
            $result     = $r['result'];
        }
        return $result;
    }
}