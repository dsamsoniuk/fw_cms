<?php
/**
 * Przykładowy routing
    array(
        'track'         => 'main/{id}/{name}',
        'name'          => 'main',
        'controller'    => 'src/controller/main.php',
        'vars'          => array(
            'id'    => 'int',
            'name'  => 'string'
        ),
        'role' => 'admin'
    )
 */
abstract class track {
    /**
     * Routing array with controller, url and name function
     */
    protected $routing  = array(
        array(
            'track'         => '^/',
            'name'          => 'base',
            'controller'    => 'src/controller/base.php'
        ),
        array(
            'track'         => 'main/{id}/{name}',
            'name'          => 'main',
            'controller'    => 'src/controller/main.php',
            'vars'          => array(
                'id'    => 'int',
                'name'  => 'string'
            )        
        ),
        array(
            'track'         => 'login',
            'name'          => 'login',
            'controller'    => 'src/controller/login.php'
        ),
        array(
            'track'         => 'logout',
            'name'          => 'logout',
            'controller'    => 'src/controller/logout.php'
        ),
        array(
            'track'         => 'register',
            'name'          => 'register',
            'controller'    => 'src/controller/register.php'
        ),
        array(
            'track'         => 'translation',
            'name'          => 'translation',
            'controller'    => 'src/controller/translation.php',
            'role'          => 'admin'
        ),
        array(
            'track'         => 'translate_delete',
            'name'          => 'translation_delete',
            'controller'    => 'src/RPC/translation.php',
            'role'          => 'admin',
            'content_type'  => 'json'
        ),
        array(
            'track'         => 'translate_update',
            'name'          => 'translation_update',
            'controller'    => 'src/RPC/translation.php',
            'role'          => 'admin',
            'content_type'  => 'json'
        )
    );
    /**
     * Default controllers without routing
     */
    protected $routing_def = array(
        'error' => array(
            'name'          => 'error404',
            'controller'    => 'src/controller/error404.php'
        )
    );
}