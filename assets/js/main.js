console.log('1232 test');


window.callAjax = function (track, data, callback) {

    $.ajax({
        url: main_dir + track,
        data: data,
        success: function(result){
            callback( result );
    }});

};

var trans = {
    delete: function(btn, id) {
        callAjax('/translate_delete=',{id:id}, function(r) {
            if (r.success) {
                $(btn).closest('tr').remove();
            } 
        });
    },
    edit: function(btn) {

        var box = $(btn).closest('tr');
        var content = box.find('.trans_content').html();
        var box_edit = '<span> \
            <textarea  class="form-control new_trans_content"  cols="30" rows="3">'+content+'</textarea><br>\
            <button onclick="trans.update(this);" class="btn btn-success">zaktualizuj</button> \
        </span>';
        box.find('.trans_content').html(box_edit);
    },
    update: function(btn) {
        var box = $(btn).closest('td');
        var id = box.attr('id');
        var content = box.find('textarea').val();

        callAjax('/translate_update=',{
            id: id.replace('content_trans_','', id),
            content: content
        }, function(r) {
            if (r.success) {
                box.html(content);
            }
        });

    }
};