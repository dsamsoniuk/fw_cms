-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Czas generowania: 04 Gru 2018, 11:23
-- Wersja serwera: 5.7.19
-- Wersja PHP: 7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `test`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `permission`
--

DROP TABLE IF EXISTS `permission`;
CREATE TABLE IF NOT EXISTS `permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `description` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Zrzut danych tabeli `permission`
--

INSERT INTO `permission` (`id`, `name`, `description`) VALUES
(1, 'watch_page', 'Dostep do jakiejs podstrony');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `permission2role`
--

DROP TABLE IF EXISTS `permission2role`;
CREATE TABLE IF NOT EXISTS `permission2role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `permission_id` (`permission_id`),
  KEY `role_id` (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `role`
--

DROP TABLE IF EXISTS `role`;
CREATE TABLE IF NOT EXISTS `role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Zrzut danych tabeli `role`
--

INSERT INTO `role` (`id`, `name`) VALUES
(1, 'admin'),
(2, 'user');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `translation`
--

DROP TABLE IF EXISTS `translation`;
CREATE TABLE IF NOT EXISTS `translation` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(1000) NOT NULL,
  `lang` varchar(100) NOT NULL,
  `content` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

--
-- Zrzut danych tabeli `translation`
--

INSERT INTO `translation` (`id`, `name`, `lang`, `content`) VALUES
(1, 'main.pierwszytekst', 'pl', 'Moj piereszy przetlumaczony tekst'),
(2, 'basic.tlumaczenia', 'pl', 'TÅ‚umaczenia'),
(8, 'main.panel.logowania', 'pl', 'Panel logowania'),
(7, 'main.rejestracja', 'pl', 'Rejestracja'),
(9, 'main.title', 'pl', 'DS Framework'),
(10, 'main.wyloguj', 'pl', 'Wyloguj siÄ™'),
(11, 'main.witaj', 'pl', 'Witaj!'),
(12, 'main.niezalogowany', 'pl', 'Zaloguj siÄ™'),
(17, 'basic.menu', 'pl', 'Menu'),
(18, 'basic.strona.glowna', 'pl', 'DS Framework'),
(19, 'basic.error404', 'pl', 'Error 404'),
(20, 'basic.strona.nieistnieje', 'pl', 'Podana przez Ciebie strona nie istnieje');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(100) NOT NULL,
  `login` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Zrzut danych tabeli `user`
--

INSERT INTO `user` (`id`, `username`, `login`, `password`, `email`) VALUES
(1, 'Jacek', 'aa', 'e0c9035898dd52fc65c41454cec9c4d2611bfb37', ''),
(5, 'Pawcio', 'ee', '1f444844b1ca616009c2b0e3564fecc065872b5b', 'ee@ww.ww'),
(6, 'Damian', 'cc', '08af6d89cb7339715969fa538e7369d8bd564926778ee1813270323427d68607', 'cc@cc.pl');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `user2role`
--

DROP TABLE IF EXISTS `user2role`;
CREATE TABLE IF NOT EXISTS `user2role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `role_id` (`role_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Zrzut danych tabeli `user2role`
--

INSERT INTO `user2role` (`id`, `user_id`, `role_id`) VALUES
(1, 1, 2),
(2, 1, 1),
(3, 6, 2),
(4, 6, 1);

--
-- Ograniczenia dla zrzutów tabel
--

--
-- Ograniczenia dla tabeli `permission2role`
--
ALTER TABLE `permission2role`
  ADD CONSTRAINT `permission2role_ibfk_1` FOREIGN KEY (`permission_id`) REFERENCES `permission` (`id`),
  ADD CONSTRAINT `permission2role_ibfk_2` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`);

--
-- Ograniczenia dla tabeli `user2role`
--
ALTER TABLE `user2role`
  ADD CONSTRAINT `user2role_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`),
  ADD CONSTRAINT `user2role_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
