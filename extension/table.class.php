<?php

class table {

    public $head = array();
    public $body = array();

    function setRowHead($col){
        foreach ($col as $box) {
            $this->head[] = array($box[0], $box[1] );
        }
    }

    function setRowBody($col){
        $row = array();
        foreach ($col as $box) {
            $row[] = $box;
        }
        $this->body[] = $row;
    }

    function generate(){

        $html = '<table class="table" style="background-color: #ffffffb0">';
        $html .= "<thead><tr>";
        foreach ($this->head as $col) {
            if (!$col[1])
            $html .= '<th>'.$col[0]."</th>"; 
            else
            $html .= '<th class="col-md-'.$col[1].'">'.$col[0]."</th>"; 
        }
        $html .= "</thead></tr>";
        $html .= "<tbody>";

        foreach ($this->body as $col) {
            $html .= "<tr>";
            foreach ($col as $c) {
                $class_c    = (isset($c['class'])) ? $c['class'] : '';
                $id_c       = (isset($c['id'])) ? $c['id'] : '';
                $html       .= '<td class="'.$class_c.'" id="'.$id_c.'">'.$c['content']."</td>";
            }
            $html .= "<tr>";
        }
        $html .= "</tbody></tr>";
        $html .= "</table>";

        return $html;
    }
}
