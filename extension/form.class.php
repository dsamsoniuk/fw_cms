<?php

// namespace main;

class form {

    private $form_attr  = "";
    private $inputs     = array();

    function __construct($action, $method){
        $this->form_attr = '<form action="'.$action.'" method="'.$method.'" class="form">';
    }

    function setInput($name, $type, $title="", $value = "") {
        if ($type == "submit") {
            $html = '  <button type="submit" class="btn btn-default">'.$name.'</button>';
        } else {
            $html  = '<div class="form-group"><label for="exampleInputEmail1">'.$title.'</label>:<br>';
            $html .= '<input type="'.$type.'" class="form-control" name="'.$name.'" value="'.$value.'" /></div>';
        }
        $this->inputs[]     = $html;
    }

    function generate(){
        $html = "";
        $html .= $this->form_attr;
        foreach ($this->inputs as $input) {
            $html .= $input;
        }
        $html .= '</form>';
        return $html;
    }

}
