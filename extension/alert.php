<?php


function alert(string $val, string $type) {
    // alerts: success, danger,warning,info
    $alert = '<div class="alert alert-'.$type.'" role="alert">'.$val.'</div>';
    echo $alert;
}